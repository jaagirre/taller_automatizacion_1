# Ejemplos basicos de Bash Scripting

En este carpeta se ubican una serie de script basico que se pueden utilizar como base para crear scripts. Estos script recogen conceptos como paso de parametros , prompt de variables , funciones y operadores lógicos. Para probar estos scripts es suficiente con disponer de una maquina GNU/Linux. En el mudle de la asignatura teneis un documento que describe un poco estos scripts y los conceptos que trata cad auno de ellos.

A continuacion se describen sucintamenete cada script y los cocneptos que utiliza:

1. froga.sh : script de prueba. Escribe texto en la consola.

```bash
    froga.sh
```

2. crearFicheros.sh : Script para la creacion de N ficheros de texto. Ejecución de comandos bash basicos.

```bash
    crearFicheros.sh
```

3. crearFicheros.1.sh: Script para la creacion de N ficheros. Utiliza variables, for/if/else y variable de estado de ejecucion de ultimo comando bash \$?

```bash
    crearFicheros.1.sh
```

4. crearFicheros.2.sh: Script para la creacion de N ficheros chequenado que se h acreado una carpeta

```bash
    crearFicheros.2.sh
```

5. comprobar_tipo.sh : Script qu edice si el nombre que le pasamos al script es un fichero o directorio. Utiliza paso de parametros y las opciones -d -f para ver si un nombre es un fichero.

```bash
    crearFicheros.sh nombreFitxDir
```

6. copia_fotos.sh : Script que recibe el nombre de un fichero y lo copia a una carpeta determinada.

```bash
    copiar_fotos.sh nombreFitx
```

7. reemplazar_ficheros.sh : Comprueba si el numero de paranetros son pasados son dos y en ese caso copia el conetnido d eun fichero a otro. Utilización de la variable \$#

```bash
    reemplazar_ficheros.sh Fitx1 Fitx2
```

8. copia_parejas.sh : Script que comprueba si el numero de parametros pasados al script es par. En ese caso copia el contenido de un fichero en otro. Utiliza el comando shift para moverse por los parametrso de entrada de forma iterativa

```bash
    copia_parejas.sh Fitx1 Fitx2 Fitx3 Fitx4
```

9. suma.sh : Pide al usuario dos numeros y realiza la suma. Lectura de datos por teclado.

```bash
    suma.sh
```

10. installApache.sh: Script complejo que instala Apache. Utiliza opciones como parametros de entrada. Y utiliza funciones con parametros.

```bash
    instalarApache.sh -u administrador -g administrador -l /var/log/php-fpm
```

11. mongo.sh : Script complejo que instala Apache. Utiliza opciones como parametros de entrada. Y utiliza funciones con parametros.

```bash
   mongo.sh -u administrador -c secreto -n 27017
```

12. mongo_file.sh : Script complejo que instala Apache. Utiliza opciones como parametros de entrada. Utiliza funciones con parametros y fichero de configuración .ini.

```bash
     mongo_file.sh -file fichero.ini
```
