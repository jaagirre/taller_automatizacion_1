#!/bin/bash

if [ -f $1 ]; then
    echo "La entrada es un fichero"
    echo "El contenido de $1 es:"
    cat $1
elif [ -d $1 ]; then
    echo "La entrada es un directorio"
    echo "El contenido de $1 es :"
    ls -la $1
else
    echo "La entrada no es ni un fichero ni un directorio"
fi