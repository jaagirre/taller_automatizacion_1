#!/bin/bash

set -e
logger "El guion de instalacion y configuracion de Mongodb ha empezado su ejecución."
USO="Uso : instalar-mongodb.sh [opciones]
Ejemplo:
 instalar-mongodb.sh -file fichero.ini
Parametros:
  -file <fichero de entrada>
  -a    Mostrar la ayuda
Formato del fichero:
  USER='<usuario>'
  PASS='<clave>'
  PORT='<puerto>' (opcional)
"
function ayuda() {
 echo "${USO}"
 if [[ ${1} ]]
 then
  echo ${1}
 fi
}
# Leer el fichero de configuracion
echo $#
if test $# -ne 2  
then
    echo "Error, numero de parametros incorrecto"
    ayuda
    exit 1
fi

in_file=$2
echo "Fichero de entrada: ${in_file}"
source ${in_file}

if [ -z ${USUARIO} ]
then
 ayuda "El usuario (-u) debe ser especificado"; exit 1
fi
if [ -z ${CLAVE} ]
then
 ayuda "La clave (-c) debe ser especificada"; exit 1
fi
if [ -z ${PUERTO_MONGOD} ]
then
 PUERTO_MONGOD=27017
fi



echo "Installing repo"
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927

echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.2 main" > /etc/apt/sources.list.d/mongodb-org-3.2.list


if [[ -z "$(mongo --version 2> /dev/null | grep '3.2.1')" ]]
then
  echo "Installing binaries"
  apt-get update
  apt-get install -y mongodb-org  --allow-unauthenticated
else
  echo "Mongo already installed"
fi   
service mongod stop


echo "Setting up default settings"
# Crear las carpetas de logs y datos con sus permisos
#[[ -d "/datos/bd" ]] || mkdir -p -m 755 "/datos/bd"
#[[ -d "/datos/log" ]] || mkdir -p -m 755 "/datos/log"
# Establecer el dueño y el grupo de las carpetas db y log
#chown mongodb /datos/log /datos/bd
#chgrp mongodb /datos/log /datos/bd

rm -rf /var/lib/mongodb/*
cat > /etc/mongod.conf <<EOF
storage:
  dbPath: /var/lib/mongodb
  directoryPerDB: true
  journal:
    enabled: true
  engine: "wiredTiger"

systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

net:
  port: ${PUERTO_MONGOD}
  bindIp: 0.0.0.0
  maxIncomingConnections: 100

replication:
  oplogSizeMB: 128
  replSetName: "rs1"

security:
  authorization: enabled
EOF


echo 'Waiting for mongod daemon...'
service mongod start

until mongo admin --eval "db.users.find();" > /dev/null 2>&1
do
 logger "Esperando a que mongod responda..." && sleep 5
done
logger "Mongod está disponible"

mongo admin <<EOF
use admin
rs.initiate()
exit
EOF


sleep 5
echo "Adding admin user"

mongo admin <<CREACION_DE_USUARIO
use admin
rs.initiate()
var user = {
  "user" : "${USUARIO}",
  "pwd" : "${CLAVE}",
  roles : [
      {
          "role" : "userAdminAnyDatabase",
          "db" : "admin"
      }
  ]
}
db.createUser(user);
exit
CREACION_DE_USUARIO

echo "Complete"