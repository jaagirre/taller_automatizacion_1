#!/bin/bash

echo "Numero de parametros: $#"

#if [ $(($# % 2)) -eq 0 ]; then
if [ $(($# % 2)) -eq 0 ]; then
    while [ $# -gt 0 ]; do
        source=$1
        shift;
        dest=$1
        shift;
        cp $source $dest
    done
else
    echo "Error, argumentos impares"
fi