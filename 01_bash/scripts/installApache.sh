#!/bin/bash
logger "El guion de instalación y configuración de PHP-FPM ha empezado su ejecución."
USO="Uso : instalar-php-fpm.sh [opciones]
Ejemplo:
instalar-php-fpm.sh -u administrador -g administrador -l /var/log/php-fpm
Opciones:
-u usuario php-pfm
-g grupo php-pfm
-l carpeta logs php-pfm
-a muestra esta ayuda
"
function ayuda() {
    echo "${USO}"
    if [[ ${1} ]]
    then
        echo ${1}
    fi
}

# Gestionar las opciones
while getopts ":u:g:l:a" OPCION
do
    case ${OPCION} in
        u ) USUARIO_PHP=$OPTARG
            logger "Parámetro USUARIO_PHP establecido con '${USUARIO_PHP}'";;
        g ) GRUPO_PHP=$OPTARG
            logger "Parámetro GRUPO_PHP establecido con '${GRUPO_PHP}'";;
        l ) CARPETA_LOGS=$OPTARG
            logger "Parámetro CARPETA_LOGS establecido con '${CARPETA_LOGS}'";;
        a ) ayuda; exit 0;;
        : ) ayuda "Falta el parámetro para -$OPTARG"; exit 1;;
        \?) ayuda "La opción no existe : $OPTARG"; exit 1;;
    esac
done
if [ -z ${USUARIO_PHP} ]
then
U   SUARIO_PHP="ubuntu"
fi
if [ -z ${GRUPO_PHP} ]
then
    GRUPO_PHP="ubuntu"
fi
if [ -z ${CARPETA_LOGS} ]
then
    CARPETA_LOGS="/var/log/php-fpm"
fi
# Instalar PHP-FPM usando apt-get o yum dependiendo de la release y el sistema operativo de forma no interactiva
if [[ -e /etc/redhat-release || -e /etc/system-release ]]
then
    OS=$(rpm -q --whatprovides redhat-release | cut -d"-" -f1)
    RELEASE=$(rpm -q --whatprovides redhat-release | cut -d"-" -f3)
case $OS in
    redhat|centos)
        yum -y install epel-release
        if [[ -z "$(yum repolist | grep 'webtatic')" ]]
        then
            if [[ "${RELEASE}" == "6" ]]
            then
                rpm -Uvh https://mirror.webtatic.com/yum/el${RELEASE}/latest.rpm
            else
                rpm -Uvh https://mirror.webtatic.com/yum/el${RELEASE}/webtatic-release.rpm
            fi
        fi
        yum install -y php55w php55w-cli php55w-common php55w-fpm php55w-mysql php55w-curl php55w-mbstring php55w-mcrypt php55w-opcache php55w-gd php55w-json php55w-xml;;
    system)
        yum -y install epel-release
        yum install -y php55 php55-cli php55-common php55-fpm php55-mysqlnd php55-mbstring php55-mcrypt php55-opcache php55-gd php55-json php55-xmlrpc;;
    *)
        echo "OS: ${OS}, Release: ${RELEASE} not supported";;
esac
yum clean all
else
    export DEBIAN_FRONTEND=noninteractive
    apt-get -y update
    apt-get -y --force-yes install php5 php5-cli php5-common php5-fpm php5-mysql php5-curl php5-mcrypt php5-gd php5-json php5-xmlrpc
fi
# Crear el archivo de configuración de PHP-FPM
(
cat << PHP_FPM_CONF
[www]
listen = /var/run/php5-fpm.sock
listen.allowed_clients = 127.0.0.1
listen.owner = ${USUARIO_PHP}
listen.group = ${GRUPO_PHP}
user = ${USUARIO_PHP}
group = ${GRUPO_PHP}
; Tipo de gestión de procesos hijos
pm = dynamic
; Número máximo de procesos hijos a ser creados por el gestor de procesos
pm.max_children = 50
; Número de procesos hijos a crear en el arranque de PHP-FPM
pm.start_servers = 5
; Número mínimo de procesos en reposo
pm.min_spare_servers = 5
; Número máximo de procesos en reposo deseado
pm.max_spare_servers = 35
; Log de Errores
php_admin_value[error_log] = ${CARPETA_LOGS}/www-error.log
php_admin_flag[log_errors] = on
; Establecer la variable session.save_path a una carpeta perteneciente al usuario del proceso
php_value[session.save_handler] = files
php_value[session.save_path] = /var/lib/php/session
PHP_FPM_CONF
) > /tmp/www.conf
if [[ -e /etc/redhat-release || -e /etc/system-release ]]
then
    mv /tmp/www.conf /etc/php-fpm.d/www.conf
else
    mv /tmp/www.conf /etc/php5/fpm/pool.d/www.conf
fi
# Dar permisos de escritura al usuario sobre el subdirectorio de logs de php-fpm
mkdir -p ${CARPETA_LOGS}
chown ${USUARIO_PHP}:${GRUPO_PHP} ${CARPETA_LOGS} -R
# Lanzamos php-fpm como demonio y si ya estaba corriendo lo matamos primero
if ! pgrep -f php-fpm > /dev/null
then
    php5-fpm --daemonize
else
    pkill -f php-fpm
    php5-fpm --daemonize
fi