#!/bin/bash

if [ $# -eq 2 ]; then
    cp -f $1 $2
else
    echo "Error, el numero de parametros introducido no es correcto"
fi