# Taller Automatizacion 1 : Bash , AWS CLI, AWS SDK, AWS Cloudformation

En este repositorio se encuentran los diferentes scriptts y plantillas para la realziación del Talle_1 sobre automatización de infraestructuras. La documentación del taller la encontrareis en el curso **Mudle** de la asignatura. La organización de las carpetas es la siguiente:

- _00_ejemplo_aws/_
- _01_bash/_
- _02_aws_cli_sdk/_
- _03_aws_cloudformation/_

En el README.md de cada carpeta teneis los pasos a seguir para ejecutar cada una de los ejercicios.

## 00_ejemplo_aws

En la carpeta **_00_ejemplo_aws/_** teneis una serie de plantillas **AWS Cloudformation** y scripts que utilizan el **AWS CLI** y **AWS SDK** que permiten desplegar automaticamente la aplicación monolitica de la asignatura AAS del master MACC y gestionar mediante scripts el arranque/parado/finalización de los recursos. En el taller lo primero que se realizará será poner en marcha este ejemplo y utilizar los diferentes scripts para entender el objetivo de este taller.

## 01_bash

En la carpeta **_01_bash/_** se ubican una serie de **BASH Scripts** basicos como introducción.

## 02_aws_cli_sdk

En la carpeta **_02_aws_cli_sdk_** se ubican una serie de sencillos scripts que demuestran como se puedengestionar recursos AWS mediante Scriptts utilizando el **CLI** y el **SDK** de AWS.

## 03_aws_cloudformation/

En la carpeta **_03_aws_cloudformation/_** tenies una serie de plantillas cloudformation AWS que os permiten definir infraestructras AWS.
