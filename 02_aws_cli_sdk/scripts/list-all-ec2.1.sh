#!/bin/bash
for region in `aws ec2 describe-regions | jq -r .Regions[].RegionName`
do
    echo "Terminating region $region instances..."
    #Se asegura que s epueda terminar via API la instancia
    aws ec2 describe-instances --region $region |\
        #jq -r .Reservations[].Instances[].InstanceId
        jq -r '.Reservations[].Instances[] | "id: " + .InstanceId + " state: " + .State.Name '
done
