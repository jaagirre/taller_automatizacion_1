IP=`curl -s checkip.amazonaws.com`
aws ec2 authorize-security-group-ingress --group-name "Bastion" --protocol tcp --port 22 --cidr $IP/32
