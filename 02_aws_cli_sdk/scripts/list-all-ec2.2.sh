#!/bin/bash
    #aws ec2 describe-instances --filters "Name=instance-state-name, Values=running" |\
    aws ec2 describe-instances |\
        #jq -r .Reservations[].Instances[].InstanceId
        jq -r '.Reservations[].Instances[] | "id: " + .InstanceId + " state: " + .State.Name 
            + " az:"  +.Placement.AvailabilityZone '
