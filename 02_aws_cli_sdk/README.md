# Ejemplos basicos AWS CLI y AWS SDK

En este carpeta se describen las tareas a realizar para conocer los cocneptos básicos sobre el CLI y el SDK de AWS. Este ejericio consta de dos apartados:

1. Laboratorio QWIKSLAB : Automating AWS Services with Scripting and the AWS CLI
2. Scripting CLI con la libreria Jq

En el mudle de la asignatura disponeis de una pequeña presentación que describe brevemente los dos ejercicios. Para poder realizar estas practicas se debe de instalara el AWS CLI y el SDK de AWS Boto3. Para ello

```bash
pip install awscli --user
pip install boto3
```

## Laboratorio QWIKSLAB :Automating AWS Services with Scripting and the AWS CLI

    Toda la información para realizar el laboratorio se incluye en el QWIKSLAB. De todas maneras en la carpeta  de *scripts* se incluye los script Python del laboratorio. A conitnuación se listan y se describen. Para ejecutar estos scripts el PC debe de disponer credenciales AWS, ya que los scripts no explicitan las access-key (Tambien se podria hacer programaticamente).

    - create-keypairs.py
        - Este script genera un par de claves denominado 'SDK'
    - cleanup-keypairs.py
        - Este script borra todas las claves SSH de una region
    - snapshotter.py
        - Este script crea snapshot de los volumenes EBS, alamcenando dos por cada volumen
    - bastion_open.sh
        - Este bash permite en el security group de un PC el acceso SSH a una IP concreta
    - bastion-close.py
        - Este script borra todas las reglas de un security group
    - Stopinator.py
        - Este script para/termina aquellas instancias tageadas con *stopinator=stop* o *stopinator=terminate*
    -highlow.py
        - Este script genera metricas Cloudwatch
    - show-credentials.sh
        - Script para ver las credenciales de la maquina EC2

## Scripting CLI con la libreria Jq

En estos ejercicios se crean unos script que utilizan el CLI de AWS en conjunto conla libreria Jq. para ejecutar etos scripts se debe de instalar la libreria Jq.

```bash
sudo apt-get install jq
jq --version
aws ec2 describe-regions | jq -r .Regions[].RegionName
```

En la carpeta _scripts_ teneis unos sencillos scripts que interactuan con recursos AWS.

    - list-all-ec2.1.sh
        - Lista las instancias de todas las regiones
    - list-all-ec2.2.sh
        - Lista las instancias en ejecución  de todas las regiones utilizando filtros.
    - terminate-all-ec2.sh
        - Este script recorre todas las regiones y elimina todas las instancias de cada región
