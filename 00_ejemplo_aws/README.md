# Ejemplo AWS CloudFormation , AWS CLI y AWS SDK

Mediante este ejemplo se desplegara en AWS la version monolitica de la aplicacion, basada en Python Flask, de la asignatura AAS de MACC. La automatización del despliegue se realiza mediante AWS CloudFormation y mediante un par de scripts. Para entender el funcionamiento del despliegue basado en Cloudformation es conveniente realizar el despliegue de la aplicación primero mediante la WEB de AWS y dsespues mediante el AWS CLI.

En esta carpeta ademas de la plantilla Cloudformation que describe la infraestructura sobre la cual se va a desplegar la aplicación Flask tambien se ubican una serie de scripts que facilitan las operaciones sobre los diferentes recursoso AWS creados por la plantilla. Por ejemplo se pueden parar/arrancar/eliminar los diferentes recursoso AWS creados , asi como finalziar el Stack Cloudformationcreado.

Para esta práctica es necesario disponer de Acceso a AWS y una credenciales de acceso:

- AWS Educate Classroom
- Cuenta AWS

## La infraestructura AWS a desplegar : Plantilla Cloudformation VPC.json

El fichero **vpc.json** describe la infraestructura en la cual se va a desplegar la aplicación. En este caso se crea lo siguiente:

- 3 subredes : 2 publicas y una 1 privada
  - 3 instancias EC2 : Todas desplegadas en la red pública
  - Bastion - Servidor NAT
  - Servidor Flask API REST
    - USER_DATA que
      - instala docker
      - clona este repositorio
      - Crea el fichero .env
      - Y arranca la aplicación mediante docker-compose
  - Mapping de AMI para algunas regiones
  - Output con algunas de las ip de los elementos desplegados
  - Parametros de la plantilla - Parametros par aacceder a este Repositorio

## Scripts de automatización

En la carpeta **_devops/_** se ubican una scripts que nos facilitaran algunas tareas de gestión de recursos AWS. A continuación se describen sucintamente los diferentes scripts:

    - createS3.sh
        - Este script permite crear un bucket y copiar a un bucket un fichero
    - createStackAWS.sh
        - Este script permite ejecutar una plantilla cloudfoundation. requiere tener en un bucket S3 la plantilla Cloudformation
    - deleteStackAWS.sh
        - Este script elimina una pila/stack de cloudformation
    - list-region-ec2.sh
        - Este script recibe un aregion como parametro de entrada y lista todos las instancias presentes en dicha region
    - list-region-ec2-tag-ip.sh
        - Este script recibe una region y un tag y su valor  como parametro de entrada y lista todos las instancias presentes en dicha region
    - start-region-tag-ec2.sh
        - Este script arranca todos las instancias de una region
    - stop-region-tag-ec2.sh
        - Este script para todos las instancias de una region con un tag
    - terminate-region-ec2.sh
        - Este script finaliza todos las instancias de una region

## Despliegue de la Aplicacion mediante la consola WEB AWS Cloudformation

Acceder via Classroom a la consola WEB de AWS. Desde aqui ir al servicio Cloudfromation. Seleccionar _Crear Stack_ y seleccionar el fichero VPC.json. Rellenar los parametros _GIT_ para que el USER*DATA de las maquinas pueda clonar automaticamente este repositorio. Tambien seleccionar el par de claves SSH que se utilizara par aconectar a las instancias EC2.Dar un nombrte al stack y ejecutar la plantilla clickando \_Crear_stack*. Esperar a que los recursos se creen. Una vez se creen los recursos ver en el pestaña **output** la IP publica del servidor Flask y realizar una peticion a la url \*/orders

> http://ip-publica:13000/orders

Una vez probado que funciona eleiminar los recursos clickando en la accion _Delete Stack_

En caso de que la pila/Stack cloudformation se cree correctamente pero el servidor no responda, conectaros a la instancia EC2 correspodniente via SSH y analizar el sistema para determinat donde ha fallado el USER_DATA.

- Ejecutar un **ls** en la carpeta /home/ec2-user para determinar si se ha clonado correctamente el repositotio
- Mirar ellog /var/log/user-data.log para mirar donde ha podido fallar
  el script del USER_DATA de la instancia

## Despliegue de la Aplicacion mediante Scripts

En este apartado se va desplegar la misma infraestructura que en el ejemplo anterior pero utilizando Scripts de automatización. Estos scripts se ubican en la carpeta **_devops/_**. Para ello es necesario disponer de un PC con sistema operativo GNU/Linux con Bash. Para disponer de un PC GNU/Linux teneis dos posibilidades utilizar la maquina virtual Basic de Vagrant que ya dispones o sino crear una instancia EC2 en Amazon conel AMI Linux de Amazon.

Para poder ejecutar los scripts es necesario instalar el AWS CLI en la maquina. Si estamos en una maquina EC2 con AMI AZM el CLI ya esta disponible sino ejecutar el siguiente comando, una vez se disponga de python y pip.

```bash
pip install awswcli --user
aws --version
```

Una vez instalado el CLI configurar las credenciales de acceso. para ello podeis ejecutar _aws configure_ o modificar el fichero _.aws/credentials_. En vuestro caso tendreis que modificar el fichero _credentials_ ya que el calssroom de Educate solo da credenciales temporales.

```bash
[default]
aws_access_key_id=ASIAY3JS3NBC
aws_secret_access_key=6T4WRtNPG7u
aws_session_token=FwoGZXIvYXdzEAwaDFcS0y3H20
```

Ahora probar que podemos utilizar el CLI , por ejemplo tratar de listar las instancias EC2 de una region.

```bash
aws ec2 describe-instances --region us-east-1
```

Una vez el entorno esta preparado ejecutamos la plantilla Cloudformation _vpc.json_. Para ejcutar primero subiremos el fichero vpc.json a un **Bucket S3**. Para ello se dispone del script

```bash
cd 00_ejemplo_aws
cd devops
./createS3.sh -m plantillastaller1
./createS3.sh -b plantillastaller1 -n ../vpc.json
./createStackAWS.sh taller12 https://plantillastaller1.s3.amazonaws.com/vpc.json
GitUser:xxxx
GitPassword: *******
SSH Key pair name: lambda
```

La salida del comando anterior es

```bash
## Waiting for stack creation...
[
    {
        "Description": "handles HTTP requests",
        "OutputValue": "ec2-3-93-35-127.compute-1.amazonaws.com",
        "OutputKey": "OrderServerPublicName"
    },
    {
        "Description": "connect via SSH from bastion host",
        "OutputValue": "10.0.2.129",
        "OutputKey": "OrderServerPrivateIp"
    },
    {
        "Description": "connect via SSH as user ec2-user",
        "OutputValue": "ec2-52-205-101-117.compute-1.amazonaws.com",
        "OutputKey": "BastionHostPublicName"
    }
]
```

Una vez creado el stack podras vel output de la creacion del stack. De todas maneras si quieres volver a ver el output ejecuta el siguiente comando.

```bash
aws cloudformation describe-stacks --stack-name taller12 --query Stacks[0].Outputs
```

Ahora mediante un curl comprobareis que el API REST est aactivo.una vez arrancado tarda unos segundo en activar el servidor web.

```bash
curl ec2-3-93-35-127.compute-1.amazonaws.com:13000/orders
```

Para elimnar el stack Cloudformation podeis ejecutar el siguiente comando.

```bash
./deleteStackAWS.sh taller12
```

## Gestión de los recursos AWS creados mediante Scripts

En este apartado vamos a para y arrancar las maquinas creadas por una plantilla Cloudformation. Para esta actividad se utilizaran scripts de la carpeta _devops/_. Para parar y arrancar utilizaremos valores de _tags_. En el cloudformation se ha agregado un tag donde el key es **System** y el valor **lab40** (tag : System=lab40). En los script de parada y arranca especificaremos los parametros de region y tag. A continuación teneis los pasos para crear las maquinas y a posteriori pararlos y volverlos a arrancar.

```bash
./devops/createStackAWS.sh taller12 https://plantillastaller1.s3.amazonaws.com/vpc.json
./devops/list-region-tag-ec2-ip.sh us-east-1 System lab40
```

Esto da como salida

```bash
Lista de las instancias de la region   us-east-1...
id: i-022202a3d96b217bb state: running
id: i-059fdf7f3a3fd1898 state: running
id: i-0ee5fd1cb5d24adb8 state: running
i-022202a3d96b217bb     ec2-3-226-47-204.compute-1.amazonaws.com        ami-035b3c7efe6d061d5
Name    OrderServer
i-059fdf7f3a3fd1898     ec2-34-237-145-154.compute-1.amazonaws.com      ami-303b1458
Name    NatServer
i-0ee5fd1cb5d24adb8     ec2-18-206-151-222.compute-1.amazonaws.com      ami-035b3c7efe6d061d5
Name    BastionHost
```

Y ahora vamos a parar las maquinas y a continuación a pararlas.

```bash
./devops/stop-region-tag-ec2.sh us-east-1 System lab40
./devops/list-region-tag-ec2-ip.sh us-east-1 System lab40
```

```bash
Lista de las instancias de la region   us-east-1...
id: i-022202a3d96b217bb state: stopped
id: i-059fdf7f3a3fd1898 state: stopped
id: i-0ee5fd1cb5d24adb8 state: stopping
i-022202a3d96b217bb     None    ami-035b3c7efe6d061d5
Name    OrderServer
i-059fdf7f3a3fd1898     None    ami-303b1458
Name    NatServer
i-0ee5fd1cb5d24adb8     ec2-18-206-151-222.compute-1.amazonaws.com      ami-035b3c7efe6d061d5
Name    BastionHost
```

Y ahora las volvemos a arrancar

```bash
./devops/start-region-tag-ec2.sh us-east-1 System lab40
./devops/list-region-tag-ec2-ip.sh us-east-1 System lab40
```

Finalmente eliminamos los recursos

```bash
./devops/deleteStackAWS.sh taller12
```

Por ultimo aqui tenemos un script capaz de terminar cualquier instancia de una region

```bash
devops/terminate-region-ec2.sh us-east-1
devops/list-region-ec2.sh us-east-1
```
