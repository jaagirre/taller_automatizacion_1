import pytest
import requests
import json



from datetime import datetime, timedelta

from application import  Session
from application.models import Order, Piece

def test_new_order():
    session = Session()
    new_order = Order(
            description='Orden de prueba',
            number_of_pieces=3,
            status=Order.STATUS_CREATED
    )
    session.add(new_order)
    session.commit()
    order = session.query(Order).get(new_order.id)
    assert order.id == new_order.id, 'Error al insertar una orden'
    session.close()
