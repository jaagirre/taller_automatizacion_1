from flask import Flask
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine
from .config import Config



engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
# la conexion de la base de datos
Session = scoped_session(
            sessionmaker(
                autocommit=False,
                autoflush=True,
                bind=engine)
        )

# Esto es una funcion factory, me permite crear varios apps
# 
def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)

# Conm es un factory aqui creamos todos los elementos del factory
# from app import routes, models, errors
    with app.app_context():
        from . import routes
        from . import models
        # Esto es una clase 
        models.Base.metadata.create_all(engine)
        return app
