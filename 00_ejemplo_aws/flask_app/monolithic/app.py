from application import create_app

app = create_app()
# Esto es para poner el contexto de la aplicacion de forma global y asi usarlo 
# en los request
app.app_context().push()

# Esto es para ejecutarlo sin unicorn
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=13000)
