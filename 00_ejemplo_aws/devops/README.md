# Scripts para operaciones de  AWS 

En esta carpeta se ubican ciertos scripts para facilitar tareas de gestion sobre los diferentes recursos AWS. Por ejemplo instancias EC2.


## Scripsts

Permitir crear un bucker y cpoiar objetos. Util pasa subir plantillas Cloudformation a S3
>createS3.sh


Permite crear un stack Cloudformartion y de salida da las ip de los diferentes elementos
> createStackAWS.sh 

Elimina un stack creado previamnte con Cloudformation
> deleteStackAWS.sh


Permite listar las instancias en una region
> list-region-ec2.sh 


Permite listar las instancias de una region y con un tag concreto. Ademas lista la direccion publica y el nombre de cada una de ellas
> list-region-tag-ec2-ip.sh 


Permite arrancar todas las instancias de una region asociadas a un tag
> start-region-tagec2.sh 


Permite parar todas las instancias de una region asociadas a un tag
> stop-region-tagec2.sh 



## Dependencies

Paquete Jq, AWS CLI

Jq
```bash
yum install jq
apt-get install jq
```

AWS CLI
```bash
apt-get install awscli
```

## Comandos adicionales AWS CLI

Informacion de stacks Cloudformation creados
```bash
aws cloudformation describe-stacks
```



