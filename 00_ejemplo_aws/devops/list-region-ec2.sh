#!/bin/bash

if [ $# -eq 1 ]; then
    echo $1
    region=$1
    echo "Lista de las instancias de la region   ${region}..."
    #Se asegura que s epueda terminar via API la instancia
    aws ec2 describe-instances --region $region |\
       
        #jq -r .Reservations[].Instances[].InstanceId
        jq -r '.Reservations[].Instances[] | "id: " + .InstanceId + " state: " + .State.Name '
else
    echo "Error, el numero de parametros introducido no es correcto"
fi


