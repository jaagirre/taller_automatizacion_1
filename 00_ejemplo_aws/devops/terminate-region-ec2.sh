#!/bin/bash

if [ $# -eq 1 ]; then
    region=$1

    echo "Terminating region $region instances..."
        #Se asegura que s epueda terminar via API la instancia
        aws ec2 describe-instances --region $region |\
            jq -r .Reservations[].Instances[].InstanceId |\
                xargs -L 1 -I {} aws ec2 modify-instance-attribute \
                --region $region \
                --no-disable-api-termination \
                --instance-id {}
        #y se termina la instancia
        aws ec2 describe-instances --region $region |\
            jq -r .Reservations[].Instances[].InstanceId |\
                xargs -L 1 -I {} aws ec2 terminate-instances \
                --region $region \
                --instance-id {}
else
    echo "Error, el numero de parametros introducido no es correcto"
    echo "Utilizar: ./terminate-region-ec2.sh region"
fi