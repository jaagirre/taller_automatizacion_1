

USO="Uso : createS3.sh [opciones]
Ejemplo:
  createS3.sh -b bucketName -n  objectName
Opciones:
 -b bucket name (must exists)
 -n FileName
 -m make bucket
 -h help
"
 
function ayuda() {
 echo "${USO}"
 if [[ ${1} ]]
 then
 echo ${1}
 fi
}

function createS3Object(){

    aws s3 cp ./$OBJECT_NAME s3://$BUCKET_NAME
    aws s3 ls s3://$BUCKET_NAME
    echo 'File uploaded'
}


# Gestionar las opciones
while getopts ":b:n:m:h" OPCION
do
 case ${OPCION} in
 b ) BUCKET_NAME=$OPTARG;
    echo "Param BUCKET_NAME '${BUCKET_NAME}' ";;
 n ) OBJECT_NAME=$OPTARG;
     echo "Param FILE_NAME '${OBJECT_NAME}' ";;
 m ) MAKE_BUCKET=$OPTARG
  echo "Param MAKE_BUCKET '${MAKE_BUCKET}'";;
 h ) ayuda; exit 0;;
 \?) ayuda "La opción no existe : $OPTARG"; exit 1;;
 esac
done

if [ -z ${MAKE_BUCKET} ]
then
    if [ -z ${BUCKET_NAME} ]
    then
        ayuda "El nombre del bucket debe ser especificado o crear el bucket previamente"; exit 1
    fi
    if [ -z ${OBJECT_NAME} ]
    then
        ayuda "El nombre del fichero debe de ser especificado"; exit 1
    fi
    createS3Object
else
    echo 'Has seleccionado crear bucket'
    echo $MAKE_BUCKET
    aws s3 mb s3://$MAKE_BUCKET
    aws s3 ls s3://$MAKE_NAME
   
fi









