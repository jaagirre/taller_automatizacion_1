#!/bin/bash

if [ $# -eq 3 ]; then
    echo $1
    region=$1
    tag=$2
    value=$3
    echo "Lista de las instancias de la region   ${region}..."
    #Se asegura que s epueda terminar via API la instancia
    aws ec2 describe-instances --region $region \
        --filter Name=tag:$tag,Values=$value |\
       
        #jq -r .Reservations[].Instances[].InstanceId
        jq -r '.Reservations[].Instances[] | "id: " + .InstanceId + " state: " + .State.Name'
     aws ec2 describe-instances --region $region \
        --filter Name=tag:$tag,Values=$value  |\
        
        jq -r .Reservations[].Instances[].InstanceId |\
            xargs -L 1 -I {} aws ec2 start-instances \
            --region $region \
            --instance-id {}
    
    

    # Aqui ahora habria que esperar hasta que esten activos
     echo "Esperando a que arranquenlas instancias..."
    while [[ `aws ec2 describe-instances --region $region \
        --filter Name=tag:$tag,Values=$value --query Reservations[].Instances[].State.Name` != *"running"* ]]
    do
	    sleep 10
        echo "Waiting to start instances"
        aws ec2 describe-instances --region $region \
        --filter Name=tag:$tag,Values=$value --query Reservations[].Instances[].State.Name 
    done


     aws ec2 describe-instances --region $region \
        --filter Name=tag:$tag,Values=$value  |\
        
        jq -r .Reservations[].Instances[].InstanceId | 
        xargs -L 1 -I {}  aws ec2 describe-instances   \
            --region $region \
            --instance-id {} \
            --query 'Reservations[*].Instances[*].[InstanceId,NetworkInterfaces[0].Association.PublicDnsName, ImageId,Tags[?Key==`Name`]]' \
            --output text
else
    echo "Error, el numero de parametros introducido no es correcto"
    echo "Utilizar: ./list-region-tag-ec2.sh region tag value"
fi


