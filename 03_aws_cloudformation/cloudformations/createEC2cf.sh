#!/bin/bash
mykey=$(aws ec2 describe-key-pairs --query "KeyPairs[0].KeyName" --output text)
aws cloudformation create-stack --stack-name ec2froga \
    --template-url https://mgep-master-automation.s3.amazonaws.com/cloudformation/EC2firstExample.1.yaml \
    --parameters ParameterKey=KeyName,ParameterValue=$mykey

while [[ `aws cloudformation describe-stacks --stack-name ec2froga --query Stacks[0].StackStatus` != *"COMPLETE"* ]]
do
	sleep 10
done
aws cloudformation describe-stacks --stack-name ec2froga --query Stacks[0].Outputs