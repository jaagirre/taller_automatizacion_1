# Ejempos basicos de AWS Cloudformation

En esta carpeta teneis una serie de ejericios que os ayudaran a conocer los conceptos básicos que ofrecen las plantillas AWS Cloudformation. En el mudle de la asignatura teneis una presentación que os puede guiar en la comporesnión de estos ejericios y con los conceptos basicos de Cloudformation.

Para trabajar los concceptos basicos de Cloudformation en esta carpeta se plantean los siguientes ejercicios.

1. Creacion de un Bucket S3 mediante Cloudformation
2. Creacion de una instancia EC2 medinate Cloudformation
3. Laboratorios QWIKSLAB
4. Despliegue y gestion de la versiónMonolitica de AAS **00_ejemplo_aws**
5. Cloudformation multifichero. Wordpress con High Availability

## Creacion de un Bucket S3 via Cloudformation :s3simple.yaml

En este ejericio se creara un sencillo bucket S3 via Cloudformation. El cloudformartion se ejecutara desde la consola. Parea ejecutar los cloudformation las plantillas deben de estar alojadasa en S3. Por eso en esta practica primero se subira la **plantilla yaml** a S3 (**_s3simple.yaml_**)y despues se ejecutara el cloudformation. El objetivo de este ejercicio es conoccer los comandos CLI para ejecutar una plantilla Cloudformation.

Creación del bucket donde se guardara el la plantilla CloudFormation y subida de la plantilla a S3 **s3simple.yaml**. Para qu etodo el mundo pueda utilizar la plantilla le daremos acceso a publico al Bucket aplicando una politica de Bucket. la politica del bucket esta en **bucketpolicy.json**.

```bash
cd cloudformations
aws s3 mb s3://mgep-master-automation
aws s3 cp s3simple.yaml s3://mgep-master-automation
aws s3 ls s3://mgep-master-automation
aws s3api put-bucket-policy --bucket mgep-master-automation --policy file://bucketpolicy.json

```

Una vez subida la plantilla podemos ejecutar la plantilla para que cree un bucket y nos diga la URL del Bucket creado. Y una vez creado el stack de Cloudformation , podremos preguntar por las salidas de dicho Stack Cloudformation en cualquier momemto.

```bash
aws cloudformation create-stack --stack-name s3simple --template-url https://mgep-master-automation.s3.amazonaws.com/s3simple.yaml
aws cloudformation describe-stacks --stack-name s3Simple --query Stacks[0].StackStatus
aws s3 ls
```

Con Cloudformation es muy facil eliminar todos los recursos creados. Simplemente se ejecuta la accion delete Stack y AWS eliminara todos los recursos creados por el stack.

```bash
aws cloudformation delete-stack --stack-name s3simple
aws s3 ls
```

## Creacion de una instancia EC2: EC2firstExample.yaml

En este ejercicio crearemos una instancia EC2 junto con un security group. Se le asociara un AMI a la instancia , y se ubica la instancia en la VPC y subredpor defecto con IP dinamica. La AMI se asigna con un mapeo de AMI por region. Tambien requiere como entrada un parametro que especifica el nombre de la clave de acceso SSH a asociar a l ainstancia. La plantilla cloudformation se denomina **EC2firstExample.yaml**. Para este ejercicio previamente teneis que subir a vuestro buecket S3 la plantilla Cloudformation. En los siguientes pasos se supone que ya esta en S3 la plantilla. En los script que despliegan el Cloudformation la url S3 de la plantilla es https://mgep-master-automation.s3.amazonaws.com/cloudformation/EC2firstExample.1.yaml. Vosotros tendresi que modificar estevalor a vuestro objeto S3.

```bash
createEC2cf.sh
```

Si despues quisisreamos actualizar el cloudformation se dispone del siguiente comando AWS CLI. Para probar esta caracteritsica podeis probar primeroa no abrir un puerto en el security group y luego a abrirlo.

```bash
aws cloudformation update-stack --stack-name ec2froga \
    --template-url https://mgep-master-automation.s3.amazonaws.com/cloudformation/EC2firstExample.1.yaml \
    --parameters ParameterKey=KeyName,ParameterValue=$mykey
```

## Laboratorios QWIKSLAB

En este apartado se recomienda la realización de 3 laboratiors de qwikslabs.

1. Introduction to AWS CloudFormation
2. Creating an Amazon Virtual Private Cloud (VPC) with AWS CloudFormation
3. Launching and Managing a Web Application with AWS CloudFormation

## Despliegue y gestion de la versión monolitica de la aplicacion AAS **00_ejemplo_aws**

En la carpeta **00_ejemplo_aws** teneis todos los recursos y pasos, scripts a ejecutar para despeglar sencillamente la versión monolitica de la aplicación de la asignatura AAS en un VPC con tres instancias y docker-compose.

## Cloudformation multifichero. Wordpress con High Availability

Este cloudformation esta divido en varios ficheros para si mejorar su legibilidad, mantenimiento y resubailidad. En este ejemplo se crean los siguientes recursos , describiendose cada uno de ellos en un fichero diferente:

    - Roles IAM
    - VPC
    - Instancias EC2
    - ELB
    - Autoscalling
    - Cloudfront
    - RDS
    - S3
    - Cloudwatch
    - Route53
    - Security Groups y ACL

Para poder ejecutar en su totalidad este Cloudformation debeis de tener uan cuenta AWS con acceso IAM , un dominio en Route53 y un certificado ACM de AWS. Si se dispone de estas caracteristicas podeis ejecutar sin modificar la plantilla. En caso contrario modifcarlo para que funcione. Por ejemplo si no queremos utilizar el certificado no utilizaremos HTTPS, sin no tenemos dominio no se utilizar el servicio R53 y dependiendo de los priviliegios IAM crearemos politicas de acceso o no.

Esta plantilla es una granbase para crear cualquier tipo de infraestructuras y ademas esta divido en varios ficheroa. Todo slos ficheros se encuentran bajo la carpeta **HA_template**.

En la presentacion de mudle teneis un ejemplo con los pasos dados para poner en marcha esta plantilla donde se puede visualizar tambien los comando y las salidas d esu ejecución. A continuación resumimos los pasos a dar.

En este repo disponeis del códigoconfigurado con mis parametros para realziar el despliegue. Tambien podeis clonar el repositorio con las plantillas Cloudfromation (Este ejemplo se basa en uno de los capitulos del libro **AWS Automation Cookbook**). Una vez disponeis de las plantillas subirlas a S3 , obtener el certificado ACM y ejecutar el Cloudformation. Para facilitar el proceso podeis hacerlo via WEB. La plantilla raiz es el fichero **master.ymal**.Esta plantilla tiene referecia a los demas ficheros.

Comandos para subir las plantillas a S3:

```bash
aws s3 sync ./03_aws_cloudformation/cloudformations/HA_template/ s3://mgep-master-automation/cloudformation
aws acm list-certificates
```

Y ahora solo queda lanzar la plantilla master.yaml!!
